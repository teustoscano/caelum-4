class Funcionario{

	public String nome;
	public String departamento;
	Data dataEntrada;
	public double salario;
	public String rg;

	
	void recebeAumento(double aumento){
		this.salario =+ aumento;
	}

	double calculaGanhoAnual(){
		return salario * 12;
	}

	void mostra(Data data){
		System.out.println("Nome: "+this.nome);
		System.out.println("Departamento: "+this.departamento);
		System.out.println("RG: "+this.rg);
		System.out.println(data.formatada(dataEntrada));
		System.out.println("Salario: "+this.salario);
		System.out.println("Ganho Anual:"+this.calculaGanhoAnual());
	}

}
